# Community Tests für die dritte Hausübung der AuD 2021

Zum Ausführen der Tests sollte eine [eigene JUnit Run Configuration](https://git.rwth-aachen.de/groups/aud-tests/-/wikis/JUnit-Run-Configuration) angelegt werden, da durch den gradle task nicht alle Meldungen angezeigt werden (z.B. warum Tests ignoriert werden).

Mit * markierte Methoden testen, ob die jeweilige Klasse bzw. Interface korrekt definiert ist. Sie sind an sich keine Testmethoden und nur in Verbindung mit "richtigen" Tests brauchbar. Klassen ohne eine einzige Testmethode werden von JUnit nicht verwendet, weswegen manche eine leere mit `@Test` annotierte Methode besitzen (z.B. [`FunctionToIntTest`](#functiontointtest)).

DISCLAIMER: Das Durchlaufen der Tests ist keine Garantie dafür, dass die Aufgaben vollständig korrekt implementiert sind.

<br>

## FunctionToIntTest

### .checkInterface()* / .interfaceDefinitionCorrect()
Überprüft, ob die Definition des Interfaces `FunctionToInt` korrekt ist. Testet, dass …
- die Klasse generisch ist und einen Typparameter T hat
- die im Übungsblatt vorausgesetzten Methoden `sizeOfAlphabet()` und `apply(T)` vorhanden und richtig implementiert sind

<br>

## UnicodeNumberOfCharIndexTest

Setzt voraus, dass `FunctionToInt` richtig definiert ist.

### .checkClass()*
Überprüft, ob die Definition der Klasse `UnicodeNumberOfCharIndex` korrekt ist. Testet, dass …
- die Klasse nicht generisch ist
- die Klasse das Interface `FunctionToInt<Character>` implementiert und nicht abstrakt ist

### .testSizeOfAlphabet()
Überprüft, ob die Methode den korrekten Wert für die Größe des Alphabets zurückgibt.

### .testApply(int, char)
Überprüft, ob die Methode den korrekten Wert für einen gegebenen `char` zurückgibt.

<br>

## SelectionOfCharsIndexTest

Setzt voraus, dass `FunctionToInt` richtig definiert ist.

### .checkClass()*
Überprüft, ob die Definition der Klasse `SelectionOfCharsIndex` korrekt ist. Testet, dass …
- die Klasse nicht generisch ist
- die Klasse das Interface `FunctionToInt<Character>` implementiert und nicht abstrakt ist
- die Klasse das Attribut `theChars` korrekt definiert

### .testSizeOfAlphabet()
Überprüft, ob die Methode den korrekten Wert für die Größe des Alphabets zurückgibt.

### .testApply(int, char)
Überprüft, ob die Methode den korrekten Wert für einen gegebenen `char` zurückgibt.

<br>

## EnumIndexTest

Setzt voraus, dass `FunctionToInt` richtig definiert ist.

### .checkClass()*
Überprüft, ob die Definition der Klasse `EnumIndex` korrekt ist. Testet, dass …
- die Klasse generisch und der Typparameter T auf Enumerationen eingeschränkt ist
- die Klasse das Interface `FunctionToInt<T>` implementiert und nicht abstrakt ist

### .testSizeOfAlphabet()
Überprüft, ob die Methode den korrekten Wert für die Größe des Alphabets zurückgibt.

### .testApply(Enum)
Überprüft, ob die Methode den korrekten Wert für eine gegebene Konstante der verwendeten Enumeration zurückgibt.

<br>

## PartialMatchLengthUpdateValuesTest

Setzt voraus, dass `FunctionToInt` richtig definiert ist.

### .checkClass()* / .classDefinitionCorrect()
Überprüft, ob die Definition der Klasse `PartialMatchLengthUpdateValues` korrekt ist. Testet, dass …
- die Klasse generisch ist und einen Typparameter T hat
- die Klasse abstrakt ist
- die Klasse ein Attribut hat, das die Vorgaben auf dem Übungsblatt erfüllt
- die abstrakte Methode `getPartialMatchLengtUpdate(int, T)` und die normale Methode `computePartialMatchLengthUpdateValues(T[])` korrekt implementiert sind

<br>

## PartialMatchLengthUpdateValuesAsMatrixTest

Setzt voraus, dass `FunctionToInt` und `PartialMatchLengthUpdateValues` richtig definiert sind.

### .checkClass()*
Überprüft, ob die Definition der Klasse `PartialMatchLengthUpdateValuesAsMatrix` korrekt ist. Testet, dass …
- die Klasse generisch ist und einen Typparameter T hat
- die Klasse von `PartialMatchLengthUpdateValues` erbt und nicht abstrakt ist
- die Klasse ein Attribut hat, das die Vorgaben auf dem Übungsblatt erfüllt (Lookup table)

### .testLookupTable(Character[], int)
Überprüft, ob die Klasse bei der Instanziierung die Lookup table richtig einrichtet.

### .testGivenLookupTableEqualsGenerated()
Überprüft, ob eine gegebene Lookup table mit der vom Konstruktor erzeugten übereinstimmt.

### .testGetPartialMatchLengthUpdate(Character[], int)
Überprüft, ob die Methode den korrekten Wert für eine gegebene Kombination aus Zustand und Zeichen zurückgibt.

<br>

## PartialMatchLengthUpdateValuesAsAutomatonTest

Setzt voraus, dass `FunctionToInt` und `PartialMatchLengthUpdateValues` richtig definiert sind.

### .checkClass()*
Überprüft, ob die Definition der Klasse `PartialMatchLengthUpdateValuesAsAutomaton` korrekt ist. Testet, dass …
- die Klasse generisch ist und einen Typparameter T hat
- die Klasse von `PartialMatchLengthUpdateValues` erbt und nicht abstrakt ist
- die Klasse ein Attribut `theStates` hat, das die Vorgaben auf dem Übungsblatt erfüllt

### .testLookupTable(Character[], int)
Überprüft, ob die Klasse bei der Instanziierung die Übergänge für den Automaten richtig einrichtet.

### .testGivenAutomatonEqualsGenerated()
Überprüft, ob ein gegebener Automat mit dem vom Konstruktor erzeugten übereinstimmt.

### .testGetPartialMatchLengthUpdate(Character[], int)
Überprüft, ob die Methode den korrekten Wert für eine gegebene Kombination aus Zustand und Zeichen zurückgibt.

<br>

## ComputePartialMatchLengthUpdateValuesTest

Optional, testet die Methode `computePartialMatchLengthUpdateValues(T[])` von `PartialMatchLengthUpdateValues`. Da die Klasse abstrakt ist, kann die Methode nicht auf die herkömmliche Art getestet werden und muss von Hand zugeschaltet werden.

<br>

## StringMatcherTest

Setzt voraus, dass `PartialMatchLengthUpdateValuesAsMatrix` und `PartialMatchLengthUpdateValuesAsAutomaton` richtig definiert sind.

### .checkClass()*
Überprüft, ob die Definition der Klasse `StringMatcher` korrekt ist. Testet, dass …
- die Klasse generisch ist und einen Typparameter T hat
- die Klasse nicht abstrakt ist
- die Klasse ein Attribut hat, das die Vorgaben auf dem Übungsblatt erfüllt

### .testFindAllMatchesLookupTable(List, List, List)
Überprüft, ob eine zufällige Folge von Zeichen (needle) in einer zweiten zufälligen Folge von Zeichen (stack) unter Verwendung von `PartialMatchLengthUpdateValuesAsLookupTable` gefunden wird. Es gibt keine Garantie, dass die gesuchte Folge vorkommt. Zudem können sich needles überlappen.

### .testFindAllMatchesAutomaton(List, List, List)
Überprüft, ob eine zufällige Folge von Zeichen (needle) in einer zweiten zufälligen Folge von Zeichen (stack) unter Verwendung von `PartialMatchLengthUpdateValuesAsAutomaton` gefunden wird. Es gibt keine Garantie, dass die gesuchte Folge vorkommt. Zudem können sich needles überlappen.
